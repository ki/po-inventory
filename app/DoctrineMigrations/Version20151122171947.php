<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20151122171947 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE item (item_id BIGINT AUTO_INCREMENT NOT NULL, name VARCHAR(255) DEFAULT NULL, item_code VARCHAR(255) NOT NULL, created_date DATETIME DEFAULT NULL, stock_count BIGINT DEFAULT 0 NOT NULL, PRIMARY KEY(item_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE item_stock (item_stock_id BIGINT AUTO_INCREMENT NOT NULL, item_id BIGINT DEFAULT NULL, supplier_name VARCHAR(255) DEFAULT NULL, count BIGINT UNSIGNED DEFAULT 0 NOT NULL, date DATETIME NOT NULL, INDEX item_id (item_id), PRIMARY KEY(item_stock_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE item_stock ADD CONSTRAINT FK_61914DB0126F525E FOREIGN KEY (item_id) REFERENCES item (item_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE item_stock DROP FOREIGN KEY FK_61914DB0126F525E');
        $this->addSql('DROP TABLE item');
        $this->addSql('DROP TABLE item_stock');
    }
}
