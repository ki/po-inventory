<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20151123193326 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE item CHANGE name name VARCHAR(255) NOT NULL, CHANGE stock_count stock_count BIGINT NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1F1B251EBF257463 ON item (item_code)');
        $this->addSql('ALTER TABLE item_stock CHANGE count count BIGINT NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX UNIQ_1F1B251EBF257463 ON item');
        $this->addSql('ALTER TABLE item CHANGE name name VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE stock_count stock_count BIGINT DEFAULT 0 NOT NULL');
        $this->addSql('ALTER TABLE item_stock CHANGE count count BIGINT UNSIGNED DEFAULT 0 NOT NULL');
    }
}
