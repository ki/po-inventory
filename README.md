Simple Inventory System
=========

Database Configuration
------------
duplicate file `app/config/parameters.yml.dist` into `app/config/parameters.yml`, 
after that please fill file `app/config/parameters.yml` with real data


INSTALLATION
------------
please do the command line below on console:

* ` composer update `
* ` php app/console doctrine:migrations:migrate `

CONFIGURATION WEBROOT AND .htaccess
------------
* It is recomended you setup ` web ` folder as DocumentRoot on Apache Configuration.
