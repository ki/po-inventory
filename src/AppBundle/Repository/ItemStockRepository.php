<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\Common\Collections\Criteria;

class ItemStockRepository extends EntityRepository
{
    public function findAllIn()
    {
        $criteria = new Criteria();
        $criteria->where($criteria->expr()->gt('count', 0));
        $criteria->orderBy(array('date' => 'DESC'));

        return $this->matching($criteria);
    }

    public function findAllOut()
    {
        $criteria = new Criteria();
        $criteria->where($criteria->expr()->lt('count', 0));
        $criteria->orderBy(array('date' => 'DESC'));

        return $this->matching($criteria);
    }

}