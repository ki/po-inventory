<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Event\LifecycleEventArgs;

/**
 * ItemStock
 *
 * @ORM\Table(name="item_stock", indexes={@ORM\Index(name="item_id", columns={"item_id"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ItemStockRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class ItemStock
{
    /**
     * @var string
     *
     * @ORM\Column(name="supplier_name", type="string", length=255, nullable=true)
     */
    private $supplierName;

    /**
     * @var integer
     *
     * @ORM\Column(name="count", type="bigint", nullable=false)
     * @Assert\GreaterThan(
     *     value = 0
     * )
     */
    private $count = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=false)
     */
    private $date;

    /**
     * @var integer
     *
     * @ORM\Column(name="item_stock_id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $itemStockId;

    /**
     * @var \AppBundle\Entity\Item
     *
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Item")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="item_id", referencedColumnName="item_id")
     * })
     */
    private $item;



    /**
     * Set supplierName
     *
     * @param string $supplierName
     *
     * @return ItemStock
     */
    public function setSupplierName($supplierName)
    {
        $this->supplierName = $supplierName;

        return $this;
    }

    /**
     * Get supplierName
     *
     * @return string
     */
    public function getSupplierName()
    {
        return $this->supplierName;
    }

    /**
     * Set count
     *
     * @param integer $count
     *
     * @return ItemStock
     */
    public function setCount($count)
    {
        $this->count = $count;

        return $this;
    }

    /**
     * Get count
     *
     * @return integer
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return ItemStock
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Get itemStockId
     *
     * @return integer
     */
    public function getItemStockId()
    {
        return $this->itemStockId;
    }

    /**
     * alias for getItemStockId
     * @return int
     */
    public function getId()
    {
        return $this->getItemStockId();
    }

    /**
     * Set item
     *
     * @param \AppBundle\Entity\Item $item
     *
     * @return ItemStock
     */
    public function setItem(\AppBundle\Entity\Item $item = null)
    {
        $this->item = $item;

        return $this;
    }

    /**
     * Get item
     *
     * @return \AppBundle\Entity\Item
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * @ORM\PostPersist
     */
    public function updateItemStockOnPostPersist(LifecycleEventArgs $event)
    {
        $em = $event->getEntityManager();

        $item = $em->getRepository('AppBundle:Item')->find($this->getItem()->getItemId());
        $item->setStockCount($item->getStockCount() + $this->getCount());
        $em->persist($item);
        $em->flush();

    }

    /**
     * @ORM\PrePersist
     */
    public function setDateValueOnPrePersist()
    {
        $this->date = new \DateTime();
    }

}
