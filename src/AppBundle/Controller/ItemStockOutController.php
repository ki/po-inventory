<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Item;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AppBundle\Entity\ItemStock;
use AppBundle\Form\ItemStockType;

/**
 * ItemStock controller.
 *
 * @Route("/itemstock/out")
 */
class ItemStockOutController extends Controller
{

    /**
     * Lists all ItemStock entities.
     *
     * @Route("/", name="itemstockout")
     * @Method("GET")
     * @Template("AppBundle:ItemStock:index.html.twig")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('AppBundle:ItemStock')->findAllOut();

        return array(
            'entities'       => $entities,
            'path_new_entry' => 'itemstockout_new',
            'title'          => 'Item Stock Out List!',
        );
    }
    /**
     * Creates a new ItemStock entity.
     *
     * @Route("/", name="itemstockout_create")
     * @Method("POST")
     * @Template("AppBundle:ItemStock:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new ItemStock();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        $em = $this->getDoctrine()->getManager();
        $item = $em->getRepository('AppBundle:Item')->find($entity->getItem()->getId());

        if(($item->getStockCount() - $entity->getCount()) < 0)
        {

            $formError = new FormError('Stock Out can not be greater than current stock');
            $form->addError($formError);
        }

        if ($form->isValid()) {

            $entity->setCount(-1 * $entity->getCount());

            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->addFlash('info', 'New Item Stock has been added');
            return $this->redirect($this->generateUrl('itemstockout'));
        }

        return array(
            'path_index' => 'itemstockout',
            'entity'     => $entity,
            'form'       => $form->createView(),
        );
    }

    /**
     * Creates a form to create a ItemStock entity.
     *
     * @param ItemStock $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(ItemStock $entity)
    {
        $form = $this->createForm(new ItemStockType(), $entity, array(
            'action' => $this->generateUrl('itemstockout_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new ItemStock entity.
     *
     * @Route("/new", name="itemstockout_new")
     * @Method("GET")
     * @Template("AppBundle:ItemStock:new.html.twig")
     */
    public function newAction()
    {
        $entity = new ItemStock();
        $form   = $this->createCreateForm($entity);

        return array(
            'path_index' => 'itemstockout',
            'entity'     => $entity,
            'form'       => $form->createView(),
        );
    }
}
